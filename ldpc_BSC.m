close all

I = eye(6);
C = [1 0 1 0 0; 1 0 0 1 0; 1 0 0 0 1; 0 1 1 0 0; 0 1 0 1 0; 0 1 0 0 1];
G = [I C];
I = eye(5);
C = [1 1 1 0 0 0; 0 0 0 1 1 1; 1 0 0 1 0 0; 0 1 0 0 1 0; 0 0 1 0 0 1];
H = [C I];
n = 6;

[rows_parity_bits, position_rows, columns_parity_bits, position_columns] = parities_for_decoder(H);

error_probability = 0.005;
max_error_probability = 0.5;

repetitions = 1000;

M = max_error_probability/error_probability-1;
total_error_rate = zeros(M,1);
total_bit_flips = zeros(M,1);
total_repetitions = zeros(M,1);

k = 0;
for e = error_probability:error_probability:max_error_probability
    k = k + 1;
    error_rate = 0; 
    bit_flips = 0;
    exceptions = 0;
    repetitions_counter = 0;
    for i = 1:repetitions
        codeword = rand(n, 1) > 0.5;
        encoded_codeword = mod(codeword'*G,2);
        received_codeword = channelBS(encoded_codeword, e);
        if sum(abs(received_codeword-encoded_codeword)) > 1
            bit_flips = bit_flips + 1;
        end
        [decoded_codeword,counter] = decoderBP(received_codeword, rows_parity_bits, columns_parity_bits, position_rows, position_columns, error_probability);
        error_rate = error_rate + any(encoded_codeword~=decoded_codeword);
        if counter == 100
            counter = 0;
            exceptions = exceptions + 1;
        end
        repetitions_counter = repetitions_counter + counter;
    end
    total_error_rate(k) = error_rate/repetitions;
    total_bit_flips(k) = bit_flips/repetitions;
    total_repetitions(k) = repetitions_counter/(repetitions - exceptions);
end

e = error_probability:error_probability:max_error_probability;
figure
plot(e,total_error_rate,'LineWidth',3)
hold on
plot(e, total_bit_flips,'o','LineWidth',3)
ax = gca;
ax.FontSize = 25;
legend("Error Rate", "Codewords with >1 bit flip", "Location", "Southeast");
grid on
xlabel('Bit Flip Probability')
ylabel('Percentage')

figure
plot(e,total_repetitions,'o','LineWidth',3)
ax = gca;
ax.FontSize = 25;
grid on
xlabel('Erasure Probability')
ylabel('Mean repetitions')