function [decoded] = improved_decoder_BEC(received_message, parity_rows, position_rows,gmax)
%% Function for decoding a received message according to the message
% improved passing algorithm. The output is a vector containing the decoded mesage
% It takes as an input, the received message, a vector containing message
% bits corrupted by an erasure channel where '2' indicates an erasure,
% as well as the vectors 'parity_rows' and 'position_rows' and the maximum
% number of guesses gmax. The first one contains the index of the '1's at
% each row and the second one contains the index of the position of '1's
% where there is a change of a row.
% Every variable node is characterized with a vector [a0 a1 a2 ...] where
% ai indicates if the guessed erasure bit is contained in the vector or not
size_parity = length(position_rows);                                        % Number of parity check equations

size_message = length(received_message);                                    % Message size
initial_decoded_message = zeros(size_message, gmax+1);                      % In the first row there is the initial
                                                                            % message. The next rows contain the coefficients a0 a1 a2...
                                                                            % ai = 1 if the guess bit xi is contained in the vector that
                                                                            % characterizes the variable node.
CN = zeros(size_parity,1);                                                  % Check Nodes: 0 unlabeled / 1 basic equation / 2 finished
initial_decoded_message(:,1) = received_message;

g = 0;                                                                      % Number of guesses
basic_eq = 0;                                                               % Number of basic equations
equations = zeros(gmax);                                                    % Basic equations
b = zeros(gmax,1);                                                          % b vector RHS of A*x=b
while true
    old_decoded_message = initial_decoded_message;
%% Repeat for each Check Node
    for i=1:size_parity
        % If all are labeled exit the loop
        if CN(i) ~= 0
            continue
        end

        this_parity = position_rows(i);                                     % Position of the first '1'
        % in the first row
        % Find all '1' bits in the first row
        if this_parity ~= position_rows(end)
            next_parity = position_rows(i + 1);
            length_of_parity = next_parity - this_parity;
        else
            length_of_parity = length(parity_rows(this_parity:end));
        end

        d = zeros(length_of_parity,gmax+1);

        for j=0:length_of_parity-1
            d(j+1, :) = initial_decoded_message(parity_rows(this_parity + j), :);   % values of variable nodes (a0) connected to this check node
        end


        erasures = find(d(:,1) == 2);                                       % Number of erasures contained in the message
        len_erasures = length(erasures);

        if(len_erasures == 0)
            CN(i) = 1;                                                        % label 'basic equation'
            basic_eq = basic_eq+1;
            % defining basic equation a1x1 + a2x2 ... = a0
            if basic_eq <= gmax
                coeffs = mod(sum(d,1),2);
                b(basic_eq) = coeffs(1);
                equations(basic_eq,:) = coeffs(2:end);
            end
        elseif(len_erasures == 1)
            CN(i) = 2;                                                        % label 'finished'
            % Set variable node equal to the modulo 2 sum of the other nodes.
            initial_decoded_message(parity_rows(this_parity + erasures - 1),:) = mod(sum(d,1),2);
        end

    end
    %% check if the procedure continues

    if old_decoded_message == initial_decoded_message
        if not (any(CN == 0))                                               % if everything is labeled exit while loop
            break
        elseif g < gmax                                                     % make new guess if g < gmax
            g = g + 1;
            erasures = find(received_message(:,1) == 2);
            j = erasures(randi(length(erasures)));                          % choose random erasure
            initial_decoded_message(j, 1+g)  = 1;                           % set ag = 1
            initial_decoded_message(j, 1)  = 0;                             % set a0 = 0
        else
            g = g + 1;
            break
        end
    end
end
%% solving the basic equations
if g > 0 && g <= gmax
    equations = equations(1:g,1:g);
    b = b(1:g);
    x = gflineq(equations,b,2);                                             % Solve A*x = b in GF(2)
    decoded = initial_decoded_message(:,1) + initial_decoded_message(:,2:g+1) * x; % Decoded = initial + A*x
    decoded = mod(decoded,2);
else
    decoded = initial_decoded_message(:,1);
end
end
