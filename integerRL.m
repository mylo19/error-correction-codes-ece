function [L, R, new_rate] = integerRL(l,r,n)
%% This function converts l and r from real numbers to integers.
% Takes input vectors l,r and n which is the desired size of
% a codeblock and finds the best integer vectors L and R with
% the closest rate to that of l and r.
% Outputs L, R which characterize the code used and the new rate.

lsize = size(l,2); % lmax
rsize = size(r,2); % rmax

Lint = l./(1:lsize); % li/i
Rint = r./(1:rsize); % ri/i

denom = sum(Lint);

rate = 1 - sum(Rint) / denom; % rate of l and r
% we need this rate so as the new rate does not surpass it
% this rate is the best possible

% find the smallest integer that correspond to li/ri and n
Lint = floor(Lint * n / denom);  
Rint = floor(Rint * n / denom);

% final L will be Lint + xi_l (R respectively)
% xi_l and xi_r being the fractional parts

% Optimisation for vector x

% xi_l from 1 to lsize 
% xi_r from lsize+1 to lsize+rsize

% equation 1
% the sum of L is equal to n (# of variable nodes)
Aeq(1,:) = [ones(1,lsize) zeros(1,rsize)];
beq(1) = n - sum(Lint);

% equation 2
% sum i*Li = sum j*Rj = # of edges in factor graph
Aeq(2,:) = [(1:lsize) -(1:rsize)];
beq(2) = -Aeq(2,:) * [Lint Rint].';

% x1l and x1r = 0 (no edges with degree 1)
Aeq(3,:) = zeros(1,lsize+rsize);
Aeq(3,1) = 1;
Aeq(3,lsize + 1) = 1;
beq(3) = 0;

% inequality 
% sum of L = (1-rate) * n, but new rate should not surpass the old one 
a = [zeros(1,lsize) ones(1,rsize)];
b = (1-rate) * n - sum(Rint);

% integer linear programming
% minimise a*x => maximise new rate

x = intlinprog(a,(1:(lsize+rsize)), -a, -b, Aeq, beq, zeros(1,lsize+rsize), ones(1,lsize+rsize));
x = round(x); % the output is double and is converted to int

L = Lint + x(1:lsize,1).'; % L = Lint + xl 
R = Rint + x(lsize+1:lsize+rsize,1).'; % R = Rint + xr

% new rate with integer values
new_rate = 1 - sum(R) / sum(L);

end
