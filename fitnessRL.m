function [output] = fitnessRL(gain)
%% Fitness Function for the Genetic Algorithm
% The genetic algorithm tries to minimize the output of the funtion. This 
% function calculates the rate of the Binary Erasure channel, for given
% error probability, and degree of lambda and rho.
% gain = 3D vector:
% gain(1) = error probability of the BEC
% gain(2) = degree of rho
% gain(3) = degree of lambda
% output = 1/rate (by minimizing this, we maximize the rate)

e = gain(1);
rmax = round(gain(2));
lmax = round(gain(3));

options = optimoptions('linprog','Display','none');

x = 0:0.01:1;

%% parameters for linear programming to determine lambda

fun1 = 1./(1:lmax);
A1 = zeros(length(x)+1, lmax); % is determined in the for loop
A1(end,2) = 1;
b1 = [x'/e; 0];
Aeq1 = zeros(2,lmax);
Aeq1(1,:) = ones(1,lmax);
Aeq1(1,1) = 0;
Aeq1(2,1) = 1;
beq(1) = 1;
beq(2) = 0;
lb1 = zeros(lmax,1);
ub1 = 0.7 * ones(lmax,1);

%% parameters for linear programming to determine rho

fun2 = 1./(1:rmax);
A2 = zeros(length(x), rmax); % is determined in the for loop
b2 = (x-1)';
Aeq2 = zeros(2,rmax);
Aeq2(1,:) = ones(1,rmax);
Aeq2(1,1) = 0;
Aeq2(2,1) = 1;
lb2 = zeros(rmax,1);
% beq is the same


%% initialisation of random rho

rho = rand(rmax,1);
rho(1) = 0;
rho = rho/sum(rho);

for counter=1:100

    %% calculation of optimal lambda

    rhox = zeros(size(x));
    for i=1:length(x)
        for j = 2:length(rho)
            rhox(i) = rhox(i) + rho(j)*(1-x(i))^(j-1);
        end
    end

    for row = 1:length(x)
        for column = 2:lmax
            A1(row, column) = (1 - rhox(row))^(column - 1);
        end
    end
    
    r1 = (0:rmax-1) * rho;
    b1(end) = 1 / (e * r1);

    %% Find lambda using linear programming
    
    lambda = linprog(-fun1, A1, b1, Aeq1, beq, lb1,ub1, options);

    %% Calculation of optimal rho

    lambdax = zeros(size(x));
    for i = 1:length(x)
        for j = 1:length(lambda)
            lambdax(i) = lambdax(i) + lambda(j)*x(i)^(j-1);
        end
    end

    for row = 1:length(x)
        for column = 1:rmax
            A2(row, column) = -(1 - e*lambdax(row))^(column - 1);
        end
    end

    %% Find rho using linear programming
    
    rho = linprog(fun2, A2, b2, Aeq2, beq, lb2, [], options);
    
    %% Determine output for the fitness function
    
    if sum(rho) > 0 && sum(lambda) > 0 % condition to ensure that a solution has been found
        rate = 1 - (fun2 * rho) / (fun1 * lambda);
    else
        rate = 0.00001; % if the linear progamming didn't reach any solution, we set output to a great value
    end
    output = 1/rate;
end
end