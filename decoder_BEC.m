function [decoded] = decoder_BEC(received_message, parity_rows, position_rows)
%% Function for decoding a received message according to the message
% passing algorithm. The output is a vector containing the decoded mesage
% It takes as an input, the received message, a vector containing message
% bits corrupted by an erasure channel where '2' indicates an erasure,
% as well as the vectors 'parity_rows' and 'position_rows'. The first one
% contains the index of the '1's at each row and the second one contains the
% index of the position of '1's where there is a change of a row.

%% Variable initialization
size_parity = length(position_rows);                                        % number of parity equations
size_message = length(received_message);                                    % size of each message

initial_decoded_message = received_message;                                 % Variable Nodes
CN = zeros(size_parity,1);                                                  % Check Nodes 1 if ready 0 if not

%% Message Passing algorithm
while true
    old_decoded_message = initial_decoded_message;
    % The algorithm visits all Check Nodes
    for i=1:size_parity 
        % if all Check Nodes are labeled ready, the algorithm has finished
        if CN(i) == 1
            continue
        end

        this_parity = position_rows(i);                                     % Position of the first '1'
                                                                            % in the first row                         
        % Find all '1' bits in the first row
        if this_parity ~= position_rows(end)
            next_parity = position_rows(i + 1);
            length_of_parity = next_parity - this_parity;
        else
            length_of_parity = length(parity_rows(this_parity:end));
        end
        
        d = zeros(length_of_parity,1);

        for j=0:(length_of_parity-1)
            d(j+1) = initial_decoded_message(parity_rows(this_parity + j));  % values of variable nodes connected to this check node
        end

        erasures = find(d == 2);
        len_erasures = length(erasures);
        % if there are no erasures label the check node as finished
        if(len_erasures == 0)
            CN(i) = 1;    
        % if there is only one erasure, set the variable node as the modulo
        % 2 sum of the other variable nodes
        elseif(len_erasures == 1)
         
            initial_decoded_message(parity_rows(this_parity + erasures - 1)) = mod(sum(d),2);
        end

    end
    % If there is no change in any bit after a repetition, the algorithm
    % has finished.
    if old_decoded_message == initial_decoded_message
        break
    end
end
%%
decoded = initial_decoded_message; 

end
