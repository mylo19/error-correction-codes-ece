function [finalCodeword] = channelBEC(initialCodeword, e)
%% Binary Symetrical Channel
% initialCodeword = the word that the tranceiver sends, after encoding
% e = the probability of an erasure to a bit
% finalCodeword = the word after any erasures from the channel

n = length(initialCodeword);
finalCodeword = initialCodeword;
for i = 1:n
    p = rand;
      if p <= e
          finalCodeword(i) = 2;
      end
end

