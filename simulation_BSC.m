%% Simulation for Binary Symmetric Channel 
% This simulation is for simple test of the decoder. It uses two different
% coding techniques, hamming codes and ldpc. If you want to check the error
% rate of the channel, a iretavie simulations is needed, and can be found 
% in the files hamming_simulationBSC.m and ldpc_simulationBSC.m
%% Part I - Hamming Codes

%% Define message to be sent
m = 4;
n = 2^m - 1;
k = n - m;
codeword = rand(n-m, 1) > 0.5;

%% Encode message using Hamming Codes

[H, G] = hammgen(m);

encoded_codeword = mod(codeword'*G,2);

[rows_parity_bits, position_rows, columns_parity_bits, position_columns] = parities_for_decoder(H);

%% Transmit message throuugh a binary symetric channel

flip_probability = 0.1;
received_codeword = channelBS(encoded_codeword, flip_probability);

fprintf('Number of bit flips = %d\n', sum(abs(received_codeword-encoded_codeword)))

%% Decode received message using belief propagation algorithm

decoded_codeword = decoderBP(received_codeword, rows_parity_bits, columns_parity_bits, position_rows, position_columns, flip_probability);

%% Test if the procedure was successful

if sum(abs(encoded_codeword - decoded_codeword))
    disp('FAIL')
else
    disp('SUCCESS')
end

%% Part II - LPDC

%% Define the Generator and the Parity Check Matrix

G = [1 0 0 1 1 0; 0 1 1 1 0 1];
H = [0 1 1 0 0 0; 1 1 0 1 0 0; 1 0 0 0 1 0; 0 1 0 0 0 1];
[parities_rows, position_rows, parities_columns, position_columns] = parities_for_decoder(H);

%% Message Encoding with LDPC

codeword = [1 0];
encoded_codeword = mod(codeword*G, 2);

%% Transmit message throuugh a binary symetric channel

flip_probability = 0.4;
received_codeword = channelBS(encoded_codeword, flip_probability);

fprintf('Number of bit flips = %d\n', sum(abs(received_codeword-encoded_codeword)))

%% Decoding of the received message using Belief Propagation Algorithm

decoded_codeword = decoderBP(received_codeword, parities_rows, parities_columns, position_rows, position_columns, flip_probability);

%% Test if the procedure was successful

if sum(abs(encoded_codeword - decoded_codeword'))
    disp('FAIL')
else
    disp('SUCCESS')
end



