function [parity_rows,position_rows] = factor_graph(L,R)
%% This function constructs the factor graph of the code
% and equivalently the parity check matrix.
% Takes as inputs L and R which are the vectors for
% distribution of degrees along the variable and check
% nodes respectively and outputs parity check matrix
% in the form of parity rows and position rows.
% The algorithm used is random matching of sockets.


% socket construction
sockets_num = L * (1:size(L,2))'; % sum of i*Li
var_num = sum(L); % number of variable nodes
check_num = sum(R); % number of check nodes

% first step of algorithm: randomly permute 1:sockets_num
sockets = randperm(sockets_num);

parity_rows = zeros(1,sockets_num); % column positions for 1's in parity matrix
position_rows = zeros(1,check_num); % position in parity_rows for the first element of a row

% make parity_rows vector with sockets
% every iteration is for one row of the parity matrix
degree = 0;
old_pos = 0;
for i = 1:check_num
    
    % find a degree with non-zero remaining check nodes
    while true
        degree = randi(size(R,2)); 
        if R(degree) > 0
            break;
        end
    end
    
    % find degree number of sockets
    new_pos = old_pos + degree;
    find_vec = (sockets > old_pos & sockets <= new_pos);
    
    % update parity_rows with the position of sockets found
    parity_rows(old_pos+1:new_pos) = find(find_vec);
   
    position_rows(i) = old_pos + 1;  % position where the new line begins
    old_pos = new_pos;  % updating position
    R(degree) = R(degree)-1; % updating the nodes of that degree remaining
end 

% assign variable nodes to sockets and update parity_rows
degree = 0;
old_pos = 0;
for i = 1:var_num

    % find a degree with non-zero remaining variable nodes
    while true
        degree = randi(size(L,2));
        if L(degree) > 0
            break;
        end
    end
    
    % find degree number of sockets
    new_pos = old_pos + degree;
    find_vec = (parity_rows > old_pos & parity_rows <= new_pos);
    parity_rows(find_vec) = i; % replace socket numbers with variable node number
      
    old_pos = new_pos;  % updating position
    L(degree) = L(degree)-1;  % updating the nodes of that degree remaining
end


% delete duplicate elements (same variable to the same check node)
current_check_node = 0;
for i=1:sockets_num
    
    % if this is the first element of this row, skip it
    if ismember(i,position_rows)
        current_check_node = find(position_rows == i);
        continue;
    end
    % if this is the same with the previous one, erase one of them
    if parity_rows(i) == parity_rows(i-1)
        parity_rows(i-1) = 0;
        position_rows(current_check_node+1:check_num) = position_rows(current_check_node+1:check_num) - 1;
        % all the next rows will begin in position-1 from their previous ones
    end
end

non_zero = find(parity_rows ~= 0); % find all the non-zero (non-erased elements)
parity_rows = parity_rows(non_zero); % erase the zeros from the vector

erased = find(position_rows > size(parity_rows,2)); % find if any row was deleted
position_rows(erased) = []; % remove these rows

end
