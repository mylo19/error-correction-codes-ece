% Simulation script for testing the Error Rate capabilites of the improved
% decoder with different maximum number of guesses and erasure probability
% of the BEC.
% It also measures the average time needed to decode for different maximum
% number of guesses and erasure probabilitiy of the BEC.
clear all
load('./parameters/rate_20.mat')
load('./parameters/lmax_20.mat')
load('./parameters/rmax_20.mat')
n = 100; % codeword length
gmax = 10; % maximimum number of guesses
repetitions = 1000;    % repetitions for the decoding process for each erasure probability
ER = zeros(gmax,8);    % Error rate
BER = zeros(gmax,8);   % Bit Error Rate
codeword = zeros(n,1);  
k=0;    
bits = 100*1000;
time = zeros(gmax,8);
parity_rows = cell(8,1);
position_rows = cell(8,1);
% Find the LDPC code for each erasure probability and save it in a cell
% array (vectors position_rows and parity_rows.
for erasure_prob=0.1:0.1:0.8
    k = k+2;
    [l,r,old_rate] = findrl(lmax_20(k),rmax_20(k),erasure_prob);
    [L, R, new_rate] = integerRL(l',r',n);
    [parity_rows{round(k/2)},position_rows{round(k/2)}] = factor_graph(L,R);
end

k=0;
for erasure_prob=0.1:0.1:0.8
    k=k+2;
    for g = 1:gmax % run from 1 to gmax
        errors = 0;
        berrors=0;
        for i=1:repetitions
            received_codeword = channelBEC(codeword,erasure_prob); % corrupt the codeword
            tic; % start counting the time needed to decode
            decoded = improved_decoder_BEC(received_codeword, parity_rows{round(k/2)}, ...
                position_rows{round(k/2)},g); % decode with improved decoder
            time(g,round(k/2))= toc+time(g,round(k/2));    % Add the measured time     
            errors = errors + any(decoded ~=0); % check if the codeword has been decoded correctly
            berrors = berrors + any(decoded~=0); % Find the total number of bit errors
        end
        time(g,round(k/2))=time(g,round(k/2))/repetitions % Find the average time for each number of guesses g
        ER(g,k/2) = errors / repetitions; % Find the error rate
        BER(g,k/2) = berrors/bits; % Find the bit error rate

    end
end
% Plot for each erasure probability the error rate as a function of the
% number of guesses
figure(1)
plot(1:g,ER(:,1))
hold on
plot(1:g,ER(:,2))
hold on
plot(1:g,ER(:,3))
hold on
plot(1:g,ER(:,4))
hold on
plot(1:g,ER(:,5))
hold on
plot(1:g,ER(:,6))
hold on
plot(1:g,ER(:,7))
hold on
plot(1:g,ER(:,8))
hold on
legend("EP= 0.1", "EP = 0.2","EP = 0.3","EP = 0.4","EP = 0.5","EP = 0.6","EP = 0.7","EP = 0.8")
ax = gca;
ax.FontSize = 25;
ax.TitleHorizontalAlignment = 'center';
grid on
xlabel('Number of Guesses')
ylabel('Error Rate')

% Plot for each erasure probability the average time needed to as a function of the
% number of guesses
figure(2)
plot(1:gmax,time(:,1))
hold on
plot(1:gmax,time(:,2))
hold on
plot(1:gmax,time(:,3))
hold on
plot(1:gmax,time(:,4))
hold on
plot(1:gmax,time(:,5))
hold on
plot(1:gmax,time(:,6))
hold on
plot(1:gmax,time(:,7))
hold on
plot(1:gmax,time(:,8))
hold on
legend("EP= 0.1", "EP = 0.2","EP = 0.3","EP = 0.4","EP = 0.5","EP = 0.6","EP = 0.7","EP = 0.8")
ax = gca;
ax.FontSize = 25;
ax.TitleHorizontalAlignment = 'center';
grid on
xlabel('Number of Guesses')
ylabel('Average Time (s)')

