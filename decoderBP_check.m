function [continue_decoding] = decoderBP_check(decoded_message, parity_rows, position_rows)
%% Function that returns a flag when the decoding needs to stop
% decoderBP_check performs the multiplication H*x', where H is the parity 
% matrix and x is the decoding word this far. If the result equals to 0,
% the process needs to stop and the flag is false, outherwise, the
% procedure continues
% decoded_message = the message that has been decoded this far
% parity_rows = the positions of parities in the parity check matrix
% position_rows = the positions in the parity_rows vector when a new row
% starts

total_sum = 0;
for i = 1:length(position_rows)
    if position_rows(i) ~= position_rows(end)
        position_of_parities = parity_rows(position_rows(i):position_rows(i+1)-1);
    else
        position_of_parities = parity_rows(position_rows(i):end);
    end
    total_sum = total_sum + mod(sum(decoded_message(position_of_parities)),2);
end
if total_sum ~= 0
    continue_decoding = true;
else 
    continue_decoding = false;
end
end

