% simulation script for finding the error rate of different LDPC codes and
% a simple and improved decoder. Also find the rate of each code.

clear all;
n = 100; % codeword length
gmax = 30; % maximum number of guesses gmax
load('./parameters/rate.mat')
load('./parameters/lmax.mat')
load('./parameters/rmax.mat')

N=[40 35 30 23 20 15 12 10 9 8 ];   % Right Regular sequence degrees
erasure_probability = 0.05;           % erasure probabillity step
max_erasure_prob = 0.5;               % maximum erasure probability
repetitions = 1000;                 % repetitions for the decoding process for each erasure probability
M =max_erasure_prob/erasure_probability;
M=round(M);
RR_LDPC = zeros(M,1);              % Error rate for the LDPC code of the RR sequence using the simple Decoder
findRL_LDPC = zeros(M,1);          % Error rate for the LDPC code solving the LP problem using the simple Decoder
RR_LDPC_improved = zeros(M,1);     % Error rate for the LDPC code of the RR sequence using the improved Decoder
findRL_LDPC_improved = zeros(M,1); % Error rate for the LDPC code solving the LP problem using the improved Decoder
new_rate_rr = zeros(M,1);          % Rate for the LDPC code of the RR sequence 
new_rate_findrl = zeros(M,1);      % Rate for the LDPC code solving the LP problem
k = 0;
for j = erasure_probability:erasure_probability:max_erasure_prob
    k = k+1;

    [l,r,old_rate] = findrl(lmax(k),rmax(k),j); 
    [L, R, new_rate_findrl(k)] = integerRL(l',r',n);
    [parity_rows,position_rows] = factor_graph(L,R);       % Generate the two vectors by solving the LP problem

    [l,r] = right_regular(N(k),1-j);
    [L, R, new_rate_rr(k)] = integerRL(l,r,n);
    [parity_rows1,position_rows1] = factor_graph(L,R);     % Generate the two vectors using the RR sequence
    
    rr_errors = 0;
    findRL_errors = 0;
    improved_rr_errors = 0;
    improved_findRL_errors = 0;
    codeword = zeros(n,1);
    
    for i=1:repetitions
        received_codeword = channelBEC(codeword,j);      % corrupt the inital codeword

        simple_decoded_rr = decoder_BEC(received_codeword, parity_rows1, position_rows1); %use the decoder
        rr_errors = rr_errors + any(simple_decoded_rr ~=0); % Find if the initial codeword is equal to the decoded word
        
        simple_decoded_findRL = decoder_BEC(received_codeword, parity_rows, position_rows);        
        findRL_errors = findRL_errors + any(simple_decoded_findRL ~=0);

        improved_decoded_rr = improved_decoder_BEC(received_codeword, parity_rows1, position_rows1, gmax);
        improved_rr_errors = improved_rr_errors + any(improved_decoded_rr ~=0);
       
        improved_decoded_findRL = improved_decoder_BEC(received_codeword, parity_rows, position_rows, gmax);
        improved_findRL_errors = improved_findRL_errors + any(improved_decoded_findRL ~=0);
    end

    RR_LDPC(k) = rr_errors / repetitions; % percentage of errors for the simple decoder
    RR_LDPC_improved(k) = improved_rr_errors / repetitions;
    findRL_LDPC(k) = findRL_errors / repetitions;
    findRL_LDPC_improved(k) = improved_findRL_errors / repetitions;
end

% plot error rate as a function of the erasure probability
max_error_prob=0.5;
space = erasure_probability:erasure_probability:max_erasure_prob;
figure(1)
plot(space,RR_LDPC)
hold on
plot(space,RR_LDPC_improved)
hold on
plot(space,findRL_LDPC)
hold on
plot(space,findRL_LDPC_improved)
hold on
ax = gca;
ax.FontSize = 25;
ax.TitleHorizontalAlignment = 'center';
legend("Simple RR", "Improved RR gmax="+num2str(gmax), "Simple LP", "Improved LP gmax="+num2str(gmax));
grid on
xlabel('Erasure Probability')
ylabel('Error Rate')
% plot rate of code as a function of erasure probability

figure(2)
plot(space,new_rate_rr,"*",'LineWidth',10)
hold on
plot(space,new_rate_findrl,"*",'LineWidth',10)
hold on
grid on
ax = gca;
ax.FontSize = 25;
ax.TitleHorizontalAlignment = 'center';
legend("RR", "LP")
xlabel('Erasure Probability')
ylabel('Maximum Design Rate')
