function [initial_word, counter] = decoderBP(received_message, parity_rows, parity_columns, position_rows,...
    position_columns, flip_probability)
%% Function decoderBP
% Function that implements the decoding of a codeword transmitted in a BSC,
% using the Belief Propagation Algorithm. 
%% inputs:
% received_message = the message that is received
% parity_rows = the positions of the parities in each row
% parity_columns = the positions of the parities in each column
% position_rows = the position in the Rows_Parity_bits vector where a new
%                 row starts
% position_columns = the position in the Columns_Parity_bits vector where a new
%                 row starts
% flip_probability = the probability of a bit flip during the transmission
%% outputs:
% initial_word = the decoded word
% counter = the number of repetitions that were needed for the result


%% Threshold for flip probability
if flip_probability < 0.2
    flip_probability = 0.2; % this is used because we don't want the decoder to rely too much on the initial estimations
end

%% Initial Estimation of message
size_parity = length(parity_columns);
size_message = length(received_message);

initial_word = ones(1,size_message);
initial_possibility_bit_equals_1 = zeros(size_message,1);
for i=1:size_message
    if received_message(i) == 1
        initial_possibility_bit_equals_1(i) = 1 - flip_probability;
    else
        initial_possibility_bit_equals_1(i) = flip_probability;
    end
end

%% Initilization of parameters

q0 = zeros(size_parity,1); 
q1 = zeros(size_parity,1);
Q0 = zeros(size_message,1);
Q1 = initial_possibility_bit_equals_1;
r0 = zeros(size_parity,1);
r1 = zeros(size_parity,1);

for i=1:size_parity
    temp = parity_rows(i);
    q1(i) = initial_possibility_bit_equals_1(temp);
    q0(i) = 1 - q1(i);
end

%% Start of the iretative progress
counter = 0;
while (decoderBP_check(initial_word,parity_rows, position_rows) || counter==0) && counter ~=100
    counter = counter+1;
    row_nodes = ones(length(position_rows),1);
    column_nodes = ones(length(position_columns),1);
    
    %% Vertical Step
    for i=1:size_parity
        position_of_parity = parity_columns(i);
        this_parity = position_rows(position_of_parity); % find in which row we are working
        if this_parity ~= position_rows(end) % find how many parities exist in this row
            next_parity = position_rows(position_of_parity + 1);
            length_of_parity = next_parity - this_parity;
        else
            length_of_parity = length(parity_rows(this_parity:end));
        end
        vector_with_q1 = zeros(length_of_parity,1);
        for j=0:length_of_parity-1 % save all q_mn that exist in this row
            vector_with_q1(j+1) = q1(this_parity + j);
        end
        temp = 1;
        for k=1:length_of_parity
            if k~=row_nodes(position_of_parity) 
                pos = 1 - 2*vector_with_q1(k);
                temp = temp*pos;
            end
        end
        row_nodes(position_of_parity) = row_nodes(position_of_parity) + 1; 
        r0(i) = 0.5 + 0.5*temp; % find updated r_mn
        r1(i) = 1 - r0(i);
    end
    
    %% Horizontal Step
    for i=1:size_parity
        position_of_parity = parity_rows(i);
        possibility_1 = Q1(position_of_parity);
        this_parity = position_columns(position_of_parity);  % find in which column we are working
        if this_parity ~= position_columns(end) % find how many parities exist in this column
            next_parity = position_columns(position_of_parity + 1);
            length_of_parity = next_parity - this_parity;
        else
            length_of_parity = length(parity_columns(this_parity:end));
        end
        vector_with_r1 = zeros(length_of_parity,1);
        vector_with_r0 = zeros(length_of_parity,1);
        for j=0:length_of_parity-1 % save all r_mn that participate in this column
            vector_with_r1(j+1) = r1(this_parity + j);
            vector_with_r0(j+1) = r0(this_parity + j);
        end
        temp0 = 1; temp1 = 1; temp = 1;
        for k=1:length_of_parity
            if (k ~= column_nodes(position_of_parity))
                temp0 = temp0 * vector_with_r0(k);
                temp1 = temp1 * vector_with_r1(k);
                 if temp1 < 1e-06 && temp0 < 1e-06
                     temp1 = temp1*10e+06;
                     temp0 = temp0*10e+06;
                 end
                temp = temp*temp1/temp0;
            end
        end
        column_nodes(position_of_parity) = column_nodes(position_of_parity) + 1;
        q0(i) = (1 - possibility_1)*temp0;
        q1(i) = possibility_1*temp1;
        K = 1/(q0(i) + q1(i));
        q0(i) = q0(i)*K; % find updated q_mn
        q1(i) = q1(i)*K;  
    end
    
    %% Re-Estimation of message based on the updated probabilities
    for i=1:size_message
        this_parity = position_columns(i); % find in which column we are working
        possibility_1 = Q1(i);
        if this_parity~= position_columns(end)
            next_parity = position_columns(i+1);
            length_of_parity = next_parity - this_parity;
        else
            length_of_parity = length(parity_columns(this_parity:end));
        end
        vector_with_r0 = zeros(length_of_parity,1);
        vector_with_r1 = zeros(length_of_parity,1);
        for j=0:length_of_parity -1 % save all the r_mn that can be found in this column
            vector_with_r0(j+1) = r0(this_parity + j);
            vector_with_r1(j+1) = r1(this_parity + j);
        end
        temp0 = 1; temp1 = 1;
        for k=1:length_of_parity
            temp0 = temp0*vector_with_r0(k);
            temp1 = temp1*vector_with_r1(k);
        end
        Q0(i) = (1 - possibility_1)*temp0; % calculate Q0, Q1
        Q1(i) = possibility_1*temp1; 
        K = 1/(Q0(i) + Q1(i)); % normalise 
        Q0(i) = Q0(i)*K;
        Q1(i) = Q1(i)*K;
    end
    for i = 1:length(Q0)
        if Q1(i) > Q0(i)
            initial_word(i) = 1;
        else
            initial_word(i) = 0;
        end
    end
end
end

