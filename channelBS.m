function [final_codeword] = channelBS(initial_codeword, flip_probability)
%% Binary Symetrical Channel
% initial_codeword = the word that the tranceiver sends, after encoding
% flip_probability = the probability of a bit flip from the channel
% final_codeword = the word after any bit flips from the channel

n = length(initial_codeword);
final_codeword = initial_codeword;
for i = 1:n
    test = rand;
    if test <= flip_probability
        final_codeword(i) = 1 - initial_codeword(i);
    end
end

