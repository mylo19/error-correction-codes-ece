%% Simulation for Binary Symmetric Channel 
% In this simulation we use hamming codes to find the error rate, using the
% Belief Propgation algorithm, which can be found in decoderBP.m

close all

%% Define the Hamming Code that will be used

m = 3;
n = 2^m - 1;
k = n - m;
codeword = rand(k, 1) > 0.5;
[H, G] = hammgen(m);
[rows_parity_bits, position_rows, columns_parity_bits, position_columns] = parities_for_decoder(H);

%% Parameters of the Binary Symmetric Channel

flip_probability = 0.005;
max_flip_probability = 0.5;

repetitions = 1000;

M = max_flip_probability/flip_probability-1;
total_error_rate = zeros(M,1);
total_bit_flips = zeros(M,1);
total_repetitions = zeros(M,1);

%% Main simulation for various flip probabilities

k = 0;
for e = flip_probability:flip_probability:max_flip_probability
    k = k + 1;
    error_rate = 0; 
    bit_flips = 0;
    exceptions = 0;
    repetitions_counter = 0;
    for i = 1:repetitions
        %% Encode the initial word
        codeword = rand(n-m, 1) > 0.5;
        encoded_codeword = mod(codeword'*G,2);
        %% Transmit the word through a Binary Symmetric Channel
        received_codeword = channelBS(encoded_codeword, e);
        if sum(abs(received_codeword-encoded_codeword)) > 1
            bit_flips = bit_flips + 1;
        end
        %% Decode the received word
        [decoded_codeword,counter] = decoderBP(received_codeword, rows_parity_bits, columns_parity_bits, position_rows, position_columns, flip_probability);
        error_rate = error_rate + any(encoded_codeword~=decoded_codeword);
        if counter == 100
            counter = 0;
            exceptions = exceptions + 1;
        end
        repetitions_counter = repetitions_counter + counter;
    end
    %% Save the results
    total_error_rate(k) = error_rate/repetitions;
    total_bit_flips(k) = bit_flips/repetitions;
    total_repetitions(k) = repetitions_counter/(repetitions - exceptions);
end


%% Plot the results

e = flip_probability:flip_probability:max_flip_probability;
figure
plot(e,total_error_rate,'LineWidth',3)
hold on
plot(e, total_bit_flips,'o','LineWidth',3)
ax = gca;
ax.FontSize = 25;
legend("Error Rate", "Codewords with >1 bit flip", "Location", "Southeast");
grid on
xlabel('Bit Flip Probability')
ylabel('Percentage')

figure
plot(e,total_repetitions,'o','LineWidth',3)
ax = gca;
ax.FontSize = 25;
grid on
xlabel('Bit Flip Probability')
ylabel('Mean repetitions')