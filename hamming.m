% Hamming encoder

bits_num = 3;
msg = randi(10,1,bits_num);
msg = mod(msg,2);


length = 2^bits_num-1;
H = zeros(bits_num+1,length);

% parity matrix for Hamming
for i = 1:length
    bin_repr = dec2bin(i);
    vect = str2num(bin_repr');
    vect = flip(vect);
    vect(bits_num+1) = 0;
    H(:,i) = vect;
end

H = H(1:bits_num,:);


% parity matrix in standard form

i = 2;
while 2^i < length
    temp = H(:,i + 1);
    H(:,i + 1) = H(:,2^i);
    H(:,2^i) = temp;
    i = i + 1;
end


% generator matrix

B = H(:,size(H,1)+1:size(H,2)).';
G = [eye(size(B,2)); B];

codeword = mod(G * msg.', 2);
