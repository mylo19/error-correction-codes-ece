function [Rows_Parity_bits, position_Rows, Columns_Parity_bits, position_Columns] = parities_for_decoder(H)
%% parities_for_decoder  
% Function that transforms a given matrix H to 4 vectors keeping only the
% position of the parities (1s) in the matrix
%% Input:
% H = matrix examined
%% Outputs:
% Rows_Parity_bits = the positions of the parities in each row
% position_rows = the position in the Rows_Parity_bits vector where a new
%                 row starts
% Columns_Parity_bits = the positions of the parities in each column
% position_Columns = the position in the Columns_Parity_bits vector where a new
%                 row starts

[rows, columns] = size(H);

Columns_Parity_bits = 0;
position_Columns = zeros(1,columns);

for i = 1:columns
    position_Columns(i) = length(Columns_Parity_bits);
    vector_with_1 = find(H(:,i) == 1);
    Columns_Parity_bits = [Columns_Parity_bits, vector_with_1'];
end
        
Columns_Parity_bits = Columns_Parity_bits(2:end);
position_Columns = position_Columns;

Rows_Parity_bits = 0;
position_Rows = zeros(1,rows);

for i = 1:rows
    position_Rows(i) = length(Rows_Parity_bits);
    vector_with_1 = find(H(i,:) == 1);
    Rows_Parity_bits = [Rows_Parity_bits, vector_with_1];
end

Rows_Parity_bits  = Rows_Parity_bits(2:end);
position_Rows = position_Rows;
end