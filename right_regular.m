function [l,r] = right_regular(N,R)
%% This function produces vectors lambda and rho according to the right regular sequence
% Takes input R which is the desired rate and N which is the degree of 
% r = x^N. Outputs l and r.
% Warning! Due to the nature of the algorithm many errors can occur
% (Matlab cannot do these calculations). If an error occurs, follow the
% guidelines of it.

% Initialisation
r = zeros(1,N);
r(N) = 1;     % r(x) = x^N
ar = 1 / N;   % sum ri/i, we have 1 - ar / al = R

syms x
g = 1 - (1-x)^(1/N);   % function with Taylor coefficients the elements of l
x = 0;

% Main algorithm
al = ar / (1-R);  % sum li/i, solved from 1 - ar / al = R
cff = 0;  % vector with the Taylor coefficients

% Step 2 of algorithm - solving the equation
while true    
    deriv = diff(g);    % new derivative
    new_cff = double(subs(deriv));  % value of derivative for x=0
    
    k = size(cff,2) + 1;
    new_cff = new_cff / factorial(k-1);  % Taylor coefficient
    
    % Sometimes the coefficients are so small that the equation cannot be
    % solved, because they are treated as 0
    if new_cff < 0.000001
        error("Too small coefficients! Try with smaller N");
    end
    
    cff = [cff new_cff]; % updating the coefficients vector
        
    % check
    if (1./(1:k) - al) * cff.' < 0  % if the value of the function goes below 0
        break                       % the solution stands between the last two elements
    end
    
    g = deriv;  % at next iteration we have the next derivative
end

% modifying the last element in order to solve the equation
cff(end) = ( (1./(1:k-1) - al) * cff(1:k-1).' ) / (al - 1/k); 

if sum(cff) == 0     % if the equation was not solved correctly
    error("Impossible! Try with greater N");
end

% normalising vector l
l = cff / sum(cff);
end