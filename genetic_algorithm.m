function [rmax, lmax, rate] = genetic_algorithm(e, maxLimit)
%% genetic_algorithm 
% Genetic Algorithm that calculates the optimal rate, for a Binary Erasure
% with given error probability
% e = error probability of the BEC
% rmax = degree of rho
% lmax = degree of lambda
% rate = optimal rate for the given e
 
fitness_function = @fitnessRL;
lb = [e, 4, 4];
ub = [e, maxLimit, maxLimit];
opts = optimoptions(@ga, ...
                    'PopulationSize', 20, ...
                    'MaxGenerations', 8, ...
                    'UseParallel', true);

[x, output] = ga(fitness_function, 3, [], [], [], [], lb, ub, [], opts);

rmax = round(x(2));
lmax = round(x(3));
rate = 1/output;
end